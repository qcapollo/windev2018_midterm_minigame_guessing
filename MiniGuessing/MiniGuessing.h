#pragma once

#include "resource.h"
#define BUFFERSIZE 260

typedef struct ConfigInfo {
	WCHAR title[BUFFERSIZE];
	WCHAR version[BUFFERSIZE];
	int height, width;

}AppConfigInfo;

//
AppConfigInfo LoadConfiguration() {
	AppConfigInfo info;

	WCHAR curPath[BUFFERSIZE];
	WCHAR buffer[BUFFERSIZE];
	
	GetCurrentDirectory(BUFFERSIZE, curPath);
	wcscat_s(curPath, L"\\config.ini");
	
	//get height , width
	GetPrivateProfileString(L"app-config", L"width", L"Default value", buffer, BUFFERSIZE, curPath);
	info.width = _wtoi(buffer);
	GetPrivateProfileString(L"app-config", L"height", L"Default value", buffer, BUFFERSIZE, curPath);
	info.height = _wtoi(buffer);

	//Get title and version
	GetPrivateProfileString(L"app-config", L"title", L"Default value", buffer, BUFFERSIZE, curPath);
	CopyMemory(info.title, buffer, wcslen(buffer));
	GetPrivateProfileString(L"app-config", L"version", L"Default value", buffer, BUFFERSIZE, curPath);
	CopyMemory(info.version, buffer, wcslen(buffer));
	return info;
}